object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 232
  ClientWidth = 356
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 32
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 48
    Top = 67
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 48
    Top = 98
    Width = 75
    Height = 25
    Caption = 'Apagar'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 48
    Top = 129
    Width = 75
    Height = 25
    Caption = 'Apagar tudo'
    TabOrder = 3
    OnClick = Button3Click
  end
  object ListBox1: TListBox
    Left = 200
    Top = 40
    Width = 121
    Height = 114
    ItemHeight = 13
    TabOrder = 4
    OnClick = ListBox1Click
  end
  object ButtonUpdate: TButton
    Left = 48
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 5
    Visible = False
    OnClick = ButtonUpdateClick
  end
  object ButtonSave: TButton
    Left = 48
    Top = 191
    Width = 75
    Height = 25
    Caption = 'Salvar Arquivo'
    TabOrder = 6
    OnClick = ButtonSaveClick
  end
end
