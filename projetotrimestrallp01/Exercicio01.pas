unit Exercicio01;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    ButtonInserir: TButton;
    ButtonLerArquivo: TButton;
    ButtonGravar: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;
    procedure ButtonInserirClick(Sender: TObject);
    procedure ButtonGravarClick(Sender: TObject);
    procedure ButtonLerArquivoClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    TSmartphones = class(TObject)
      marca: string;
      modelo: string;
      ano: integer;

    end;
var
  Form1: TForm1;
  smartphones_julia: TSmartphones;
  arquivo: TextFile;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
      ListBox1.Clear;
end;

procedure TForm1.ButtonGravarClick(Sender: TObject);
begin
        //Salva o objeto da lista


        ShowMessage('Gravou');

        //Grava arquivos de texto
        AssignFile(arquivo,  'smartphones.txt');
        Rewrite(arquivo);
        WriteLn(arquivo, smartphones_julia.marca);
        WriteLn(arquivo, inttostr(smartphones_julia.ano));
        WriteLn(arquivo, smartphones_julia.marca);

        CloseFile(arquivo);
end;

procedure TForm1.ButtonInserirClick(Sender: TObject);
begin
      //Instanciar
      smartphones_julia:= TSmartphones.Create;

      //Preencher valores
      smartphones_julia.marca:= Edit1.Text;
      smartphones_julia.modelo:= Edit2.Text;
      smartphones_julia.ano:= strtoInt(Edit3.Text);

      //Adiciona
      ListBox1.Items.AddObject(smartphones_julia.marca, smartphones_julia);



end;

procedure TForm1.ButtonLerArquivoClick(Sender: TObject);
var
  arquivo : TextFile;
  smartphones : TSmartphones;
begin
    //Abre arquivos

    AssignFile(arquivo, 'smartphones.txt');
    Reset(arquivo);

    //Armazena

    while not Eof (arquivo) do
    begin
        smartphones:=TSmartphones.Create;
        Readln(arquivo, smartphones.marca);
        Readln(arquivo, smartphones.modelo);
        Readln(arquivo, smartphones.ano);

        //Adiciona ListBox
        ListBox1.Items.AddObject(smartphones.marca, smartphones);

    end;
   CloseFile(arquivo);
end;

end.
