object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 285
  ClientWidth = 559
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clMaroon
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    AlignWithMargins = True
    Left = 238
    Top = 104
    Width = 49
    Height = 18
    Caption = 'Marca'
    Color = clBlack
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -15
    Font.Name = 'Century'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    AlignWithMargins = True
    Left = 350
    Top = 104
    Width = 58
    Height = 18
    Caption = 'Modelo'
    Color = clBlack
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -15
    Font.Name = 'Century'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label3: TLabel
    AlignWithMargins = True
    Left = 472
    Top = 104
    Width = 31
    Height = 18
    Caption = 'Ano'
    Color = clBlack
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -15
    Font.Name = 'Century'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label4: TLabel
    Left = 212
    Top = 24
    Width = 313
    Height = 44
    Caption = 'SMARTPHONES'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -37
    Font.Name = 'Century'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 177
    Height = 265
    ItemHeight = 13
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 212
    Top = 128
    Width = 106
    Height = 21
    TabOrder = 1
  end
  object Edit2: TEdit
    Left = 324
    Top = 128
    Width = 106
    Height = 21
    TabOrder = 2
  end
  object Edit3: TEdit
    Left = 436
    Top = 128
    Width = 106
    Height = 21
    TabOrder = 3
  end
  object ButtonInserir: TButton
    Left = 212
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 4
    OnClick = ButtonInserirClick
  end
  object ButtonLerArquivo: TButton
    Left = 428
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Ler Arquivo'
    TabOrder = 5
    OnClick = ButtonLerArquivoClick
  end
  object ButtonGravar: TButton
    Left = 324
    Top = 239
    Width = 75
    Height = 26
    Caption = 'Gravar'
    TabOrder = 6
    OnClick = ButtonGravarClick
  end
  object Button1: TButton
    Left = 324
    Top = 193
    Width = 75
    Height = 25
    Caption = 'Limpar'
    TabOrder = 7
    OnClick = Button1Click
  end
end
